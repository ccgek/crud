<?php echo" <style type='text/css'>
    TABLE {
        width: 50%;
        text-align: center;
        margin-bottom: 20px;
        border-collapse: separate;
        margin: auto;
        margin-top: 3%;
    }
    TD, TH {
        padding: 0px;


    }
    TH {
        background: lawngreen;
    }

    table tr td:last-child {border-right: 0px;}
    table tbody tr:nth-child(1n) {background: #f6f6f6;}
    table tbody tr:nth-child(2n) {background: #e6e6e6;}
    a {
        text-decoration: none;
        color: aliceblue;
        outline: none;
    }
    a:visited{
        color: oldlace;
    }
    input[type='submit'],input[type='button']{
        background-color: #4CAF50;
        border: none;
        color: white;
        padding: 7px 25px;
        text-decoration: none;
        margin: 4px 2px;
        cursor: pointer;
        border-radius: 21px;


    }
    input[type='submit']:hover{
        background-color:#5cbf2a;
    }
    input[type='submit']:active{
        position:relative;
        top:1px;
    }
    a:hover{
        color: darkgreen;
    }
    body{
        margin: 0;
    }
    .header{
        background-color: darkseagreen;
        width: 100%;
        height: 50px;


    }
    .reading_man{
        width: 72px%;
        height: 50px;
        vertical-align: middle;
    }

    .menu{
        padding-left: 20px;
    }
    .header_menu{
        vertical-align: middle;
    }
    .button{
        margin-left: 25%;
        margin-top: 1%;
    }
    .choose{
        margin: 12px 0px 6px 18px;
        padding-right: 10px;
    }
    #ch_group{
        margin: -58px 0px 21px 13%;
        width: 20%;
    }
    #ch_group_edit{
        margin-bottom: -1px;
        margin-top: 10px;
        margin-left: 15%;
        width: 15%;
        margin-top: -77px;
    }
    .show{
        margin: 4px 0px 0px 7px;
    }
    .textarea{
        margin-left: 16px;

    }
    .add_textarea{
        margin-top: 18px;
        margin-left: 10px;
        width: 270px;
    }
    .add_button{
        margin-left: 141px;
        margin-top: 10px;
    }
    .new_stud{
        margin-top: 29px;
        margin-right: 0px;
        margin-left: 22px;
        padding-right: 0px;
        padding-bottom: 0px;
        padding-left: 10px;
    }
    #add_new_stud{
        margin-right: 5px;
        margin-left: 283px;
        margin-bottom: -28px;
        width: 15%;
    }
    #ch_stud_edit{
        margin-bottom: -1px;
        margin-top: 10px;
        margin-left: 15%;
        width: 15%;
        margin-top: -77px;
    }
    #ch_group_edit_subj{
        margin-bottom: -1px;
        margin-top: -33px;
        margin-left: 15%;
        width: 15%;

    }
    .add_textarea_edit_subj{
        margin-top: 18px;
        margin-left: 10px;
        width: 270px;
    }
    .add_textarea_add_subj{
        margin-top: -27px;
        margin-left: 10px;
        width: 270px;
    }
    #ch_group_add_subj{
        margin-bottom: -1px;
        margin-top: -2px;
        margin-left: 15%;
        width: 15%;

    }


</style>";
?>